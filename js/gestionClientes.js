var clientesObtenidos;

function getClientes() {
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  // Se imprime el resulado en la consola
  request.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200){   // El 4 significa que la peticiòn ya terminò y el 200 que terminò bien
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  }
  request.open("GET",url,true);
  request.send();
}
function procesarClientes() {
  var JSONClientes = JSON.parse(clientesObtenidos);
  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  var divTabla = document.getElementById("divTablaClientes");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i = 0; i < JSONClientes.value.length; i++) {
    var nuevafila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONClientes.value[i].ContactName;

    var columnaCiudad = document.createElement("td");
    columnaCiudad.innerText = JSONClientes.value[i].City;

    var columnaBandera = document.createElement("td");
    //columnaBandera.innerText = JSONClientes.value[i].Country;

    var imgBandera = document.createElement("img");
    imgBandera.classList.add("flag");

    if(JSONClientes.value[i].Country == "UK"){
      imgBandera.src = rutaBandera+"United-Kingdom.png";
    } else{
      imgBandera.src = rutaBandera+JSONClientes.value[i].Country+".png";
    }

    columnaBandera.appendChild(imgBandera);
    nuevafila.appendChild(columnaNombre);
    nuevafila.appendChild(columnaCiudad);
    nuevafila.appendChild(columnaBandera);


    tbody.appendChild(nuevafila);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
