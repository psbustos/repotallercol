var productosObtenidos; //VAriable golbal

function getProductos() {
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request = new XMLHttpRequest();
  // Se imprime el resulado en la consola
  request.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200){   // El 4 significa que la peticiòn ya terminò y el 200 que terminò bien
      //console.log(request.responseText);
      productosObtenidos = request.responseText;
      procesarProductos();
    }
  }
  request.open("GET",url,true);
  request.send();
}

function procesarProductos() {
  var JSONProductos = JSON.parse(productosObtenidos);
  var divTabla = document.getElementById("divTablaProductos");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  //alert(JSONProductos.value[0].ProductName);
  for (var i = 0; i < JSONProductos.value.length; i++) {
    //console.log(JSONProductos.value[i].ProductName);
    var nuevafila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONProductos.value[i].ProductName;

    var columnaPrecio = document.createElement("td");
    columnaPrecio.innerText = JSONProductos.value[i].UnitPrice;

    var columnaStock = document.createElement("td");
    columnaStock.innerText = JSONProductos.value[i].UnitsInStock;

    nuevafila.appendChild(columnaNombre);
    nuevafila.appendChild(columnaStock);
    nuevafila.appendChild(columnaPrecio);


    tbody.appendChild(nuevafila);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
